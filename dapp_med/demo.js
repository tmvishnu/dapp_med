function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
var d = new Date(1558435195);
var h = addZero(d.getHours());
var m = addZero(d.getMinutes());
var s = addZero(d.getSeconds());
console.log(h + ":" + m + ":" + s)